package com.reactjs.web.entity.enums;

public enum VoteMark {
    UP,
    DOWN;
}
