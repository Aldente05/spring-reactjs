package com.reactjs.web.entity.enums;

public enum VoteModule {
    QUESTION,
    ANSWER;
}
